import os
import pickle
import face_recognition

DATASET_DIR = "D:\\Girish\\Prudential\\face_recognition_example_proj\\dataset\\lfw\\"

class FaceEncoding:
    name: str
    value: []

files = []
all_face_encodings = []
# r=root, d=directories, f = files
for r, d, f in os.walk(DATASET_DIR):
    for file in f:
        if '.jpg' in file:
            files.append(os.path.join(r, file))
for path in files:
    name = os.path.basename(os.path.dirname(path)).replace("_", " ")
    img1 = face_recognition.load_image_file(path)
    encoding = face_recognition.face_encodings(img1)
    if len(encoding) > 0:
        vector = encoding[0]
        obj = FaceEncoding()
        obj.name = name
        obj.value = vector
        all_face_encodings.append(obj)
        print(f"{len(all_face_encodings)}. working on {name}")
    if len(all_face_encodings) > 10:
        if os.path.exists('dataset_faces1.dat'):
            with open('dataset_faces1.dat', 'rb') as f:
                db = pickle.load(f)
        else:
            db = []
        with open('dataset_faces1.dat', 'wb') as f:
            db.extend(all_face_encodings)
            pickle.dump(db, f)
        all_face_encodings.clear()

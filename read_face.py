import face_recognition
import pickle
import numpy as np
import time

TOLERANCE = 0.6

class FaceEncoding:
    name: str
    value: []
# Load face encodings
t1 = time.time()
with open('dataset_faces_100K.dat', 'rb') as f:
	all_face_encodings = pickle.load(f)

t2 = time.time()
# Grab the list of names and the list of encodings
print(f"db size = {len(all_face_encodings)}")
print(f"loading data took {t2 - t1}")
names = [item.name for item in all_face_encodings ]
encodings = [item.value for item in all_face_encodings ]
face_names = names
face_encodings = np.array(encodings)

unknown_image = face_recognition.load_image_file("avril.jpg")
unknown_face = face_recognition.face_encodings(unknown_image)
t1 = time.time()

distance = np.linalg.norm(face_encodings - unknown_face, axis=1)
# result = face_recognition.compare_faces(face_encodings, unknown_face)
names_with_result = list(zip(face_names, distance))
match = list(filter(lambda m: m[1] <= TOLERANCE, names_with_result))
sorted_match = match.sort(key = lambda m: m[1])
t2 = time.time()
print(f"searching data took {t2 - t1}")
# Print the result as a list of names with True/False
print(match)